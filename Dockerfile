From tomcat

maintainer stuart

copy target/hello-scalatra.war /usr/local/tomcat/webapps/hello-scalatra.war

expose 8080
CMD ["catalina.sh", "run"]
